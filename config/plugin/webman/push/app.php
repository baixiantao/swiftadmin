<?php
return [
    'enable'       => true,
    'websocket'    => 'websocket://0.0.0.0:3131',
    'api'          => 'http://0.0.0.0:3232',
    'app_key'      => '6c07d4502768615a45d8c3193b66eb88',
    'app_secret'   => 'c395efb291683dc01472deb4e9db4bcd',
    'channel_hook' => 'http://127.0.0.1:8787/plugin/webman/push/hook',
    'auth'         => '/plugin/webman/push/auth'
];